=======
# demo_lva_analysewerkzeuge

## Run

`jupyter notebook --ip=0.0.0.0 --port=8080 --no-browser`

https://lva-analysewerkzeuge-pisco.c9users.io:8080/tree?token=2fce49ea3787ce69760a06884310ba2d23edf58ade3768ca

## Merge Notebooks

`nbmerge file_1.ipynb file_2.ipynb file_3.ipynb > merged.ipynb`

[nbmerge](https://github.com/jbn/nbmerge)

## TODOs:
* Add TOC

